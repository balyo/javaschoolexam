package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    private static int index = 0;

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            Collections.sort(inputNumbers);
        } catch (Exception | OutOfMemoryError e) {
            throw new CannotBuildPyramidException();
        }
        index = 0;
        int length = inputNumbers.size();
        int row = findRow(length);
        int column = row + (row - 1);
        if (!checkLengthList(length) || column >= 2147483647) throw new CannotBuildPyramidException();
        return fillArray(inputNumbers, column, row);
    }

    private int[][] fillArray(List<Integer> inputNumbers, int column, int row) {
        int[][] array = new int[row][column];
        int from = row - 1;
        int until = row - 1;
        for(int i = 0; i < row; i++) {
            array[i] = fillRow(inputNumbers, from, until, column);
            from--;
            until++;
        }
        return array;
    }

    private int[] fillRow(List<Integer> inputNumbers, int from, int until, int column) {
        int[] rows = new int[column];
        boolean write = false;
        for(int i = 0; i < column; i++) {

            if (i > from + 1 && i < until) {
                write = !write;
            }
            if (i == from || write || i == until) {
                if (index < inputNumbers.size()) rows[i] = inputNumbers.get(index);
                index++;
            } else rows[i] = 0;
        }
        return rows;
    }

    private int findRow(int n) {
        int k = 0;
        while (n > 0) {
            k++;
            n -= k;
        }
        return k;
    }

    private boolean checkLengthList(int length) {
        int first = 1;
        int second = first + 1;
        while (first + second < length) {
            first += second;
            second++;
        }
        if (first + second == length) return true;
        else return false;
    }
}
