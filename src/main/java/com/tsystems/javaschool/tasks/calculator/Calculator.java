package com.tsystems.javaschool.tasks.calculator;

import java.util.Scanner;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement != null && checkParenthesis(statement)) {
            statement = addSpaceBetweenDigits(statement);
            if (!checkDoubleOperations(statement)) {
                String postfix = convertToPostfix(statement);
                if (postfix != null) {
                    Double result = calculateResult(postfix);
                    if (result != null) {
                        if (!Double.isInfinite(result)) {
                            if (result % Math.round(result) == 0) return String.valueOf(Math.round(result));
                            else return String.valueOf(result);
                        }
                    }
                }
            }
        }
        return null;
    }

    private static boolean checkParenthesis(String line) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < line.length(); i++) {
            char c = line.charAt(i);
            if (c == '(') stack.push(c);
            else if (c == ')') {
                if (stack.size() > 0) stack.pop();
                else stack.push(c);
            }
        }
        return stack.size() == 0;
    }

    private static boolean checkDoubleOperations(String line) {
        Pattern pattern = Pattern.compile("[\\+|\\-|\\*|\\/][\\s]{2}[\\+|\\-|\\*|\\/]");
        Matcher matcher = pattern.matcher(line);
        return matcher.find();
    }

    private static int isHigherPrecedence(String c) {
        switch (c) {
            case "*":
            case "/":
                return 1;
            case "+":
            case "-":
                return 0;
            default:
                return -1;
        }
    }

    private static String convertToPostfix(String infix) {
        try {
            Stack<String> stack = new Stack<>();
            StringBuilder postfix = new StringBuilder();
            Scanner scanner = new Scanner(infix);
            while (scanner.hasNext()) {
                String c = scanner.next();
                if (isNotOperator(c)) {
                    postfix.append(c);
                    postfix.append(" ");
                } else if (stack.isEmpty() || stack.peek().equals("(") || c.equals("(")) {
                    stack.push(c);
                } else if (c.equals(")")) {
                    while (true) {
                        if (stack.peek().equals("(")) {
                            stack.pop();
                            break;
                        } else {
                            postfix.append(stack.pop());
                            postfix.append(" ");
                        }
                    }
                } else if (!stack.isEmpty() && isHigherPrecedence(c) > isHigherPrecedence(stack.peek())) {
                    stack.push(c);
                } else {
                    while (!stack.isEmpty() && isHigherPrecedence(c) <= isHigherPrecedence(stack.peek())) {
                        postfix.append(stack.pop());
                        postfix.append(" ");
                    }
                    stack.push(c);
                }
            }
            while (!stack.isEmpty()) {
                postfix.append(stack.pop());
                postfix.append(" ");
            }
            return postfix.toString();
        } catch (Exception e) {
            return null;
        }
    }

    private static Double calculateResult(String postfix) {
        try {
            Stack<Double> stack = new Stack<>();
            Scanner scanner = new Scanner(postfix);
            while (scanner.hasNext()) {
                String next = scanner.next();
                if (isNotOperator(next)) {
                    stack.push(Double.parseDouble(next));
                } else {
                    if (stack.size() >= 2) stack.push(doOperation(stack.pop(), stack.pop(), next));
                }
            }
            return stack.pop();
        } catch (Exception e) {
            return null;
        }
    }

    private static Double doOperation(Double d2, Double d1, String operator) {
        switch (operator) {
            case "*":
                return d1 * d2;
            case "/":
                return d1 / d2;
            case "+":
                return d1 + d2;
            case "-":
                return d1 - d2;
            default:
                return null;
        }
    }

    private static String addSpaceBetweenDigits(String line) {
        return line.replaceAll("[*]", " * ")
                .replaceAll("[/]", " / ")
                .replaceAll("[+]", " + ")
                .replaceAll("[-]", " - ")
                .replaceAll("[(]", " ( ")
                .replaceAll("[)]", " ) ");
    }

    private static boolean isNotOperator(String c) {
        return !c.equals("(") && !c.equals("+") && !c.equals("-") && !c.equals("*") && !c.equals("/") && !c.equals(")");
    }

}
